This is a image for **IronWorker** based in **Python** and whole necessary for use **Postgresql**.

This is the workflow for upload a worker to iron.io

### 1. Install the dependencies:
```
docker run --rm -v "$PWD":/charges -w /charges tingsystems/python3:postgresql pip3 install -t packages -r requirements.txt
```
The image ***tingsystems/python3:posgresql*** contains the same of this image's dependencies.

### 2. Test locally
```
docker run --rm -e "PAYLOAD_FILE=payload.json" -v "$PWD":/worker -w /worker tingsystems/python3:posgresql python3 charges.py
```
If is necessary we package our image and upload to Docker
```
docker build -t tingsystems/python3:postgresql .
```
```
docker push tingsystems/python3:postgresql
```
### Register our image with Iron

```
iron register tingsystems/python3:postgresql
```

### Package our code
```
zip -r charges.zip .
```

### Upload our code
```
iron worker upload --name charges --zip charges.zip tingsystems/python3:posgresql python3 charges.py
```