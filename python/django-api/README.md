This is a image based in ***alpine linux***  to deploy an API REST build with ***Django***.

This is the workflow

### Cloning api
We're going to clone our api in same root of Dockerfile
```
git clone git@gitlab.com:tingsystems/some-api.git api
```

### Creating script file
```
touch docker-entrypoint.sh
```
Change permissions to make it executable
```
chmod u+x docker-entrypoint.sh
```
### Build Docker's image

```
docker build -t tigsystems/some-api .
```
### Executing container
```
docker run --name=some-api \
	--detach=true \
    --restart=always \
	--memory=200mb \
	--publish=8020:8000 \
	tingsystems/some-api:latest \
	--workers=1
```

### Some commands for docker
***Monitoring docker containers***
```
docker stats some-api
```

***Show containers running***
```
docker ps -a
```

***Show containers running***
```
docker ps -a
```

***Show logs***
```
docker logs -f some-api
```

***Stop all containers***
```
docker stop $(docker ps -a -q)
```

***Remove all containers***
```
docker rm $(docker ps -a -q)
```